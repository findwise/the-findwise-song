#!/bin/bash

function sing_lyric() {
  if which espeak; then
    espeak $2
  elif which say; then
    say $2
  else
    echo "You need to install say (Mac) or espeak (Unix)"
  fi
}

sing_lyric < <(curl -fsSL https://bitbucket.org/findwise/the-findwise-song/raw/db27a4814069f1adf5c26f99c8a69ad50c1309da/lyrics/findwise_song.lyrics)
