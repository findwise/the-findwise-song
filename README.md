# The FINDWISE song

The Kickoff Cabaret song made famous by **Team Awesome**.

Meidi, Jessica, Tim, Carl, Mattias, Mickaël, Kasper

Original song: Friday - Rebecca Black
[Karaoke version](http://www.youtube.com/watch?v=MaQmm9IDPV4)

## Installation

### Unix/Mac

No need for any installation, just run this command *(Requires `say` or `espeak`)*

```shell
bash -c "$(curl -fsSL https://bitbucket.org/findwise/the-findwise-song/raw/master/findwise_song.sh)"
```

#### Shortest command yet.

```shell
bash -c "$(curl -L goo.gl/T1pnS0)"
```

### Windows

To use this in Windows, you have to download the `findwise_song.vbs` to your local drive and open it from the windows terminal a.k.a *cmd* I think.

```shell
findwise_song.vbs
```

## Lyrics

Lyrics can be found in the [/lyrics](./the-findwise-song/src/master/lyrics) directory.
