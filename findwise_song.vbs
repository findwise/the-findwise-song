' terminal_sing.vbs
' A fork of Emil Stjerneman's (BratAnon) terminal sing
' https://github.com/bratanon/terminal-sing
'
' Start the script from the terminal and give a lyric name as frist argument
' findwise_song.vbs lyricname

Dim xHttp

If Wscript.Arguments.Count < 1 Then
	Wscript.Echo "You have to start from the terminal."
	Wscript.Quit
End If

Set ObjVoice = CreateObject("SAPI.SpVoice")
Set xHttp = createobject("Microsoft.XMLHTTP")

ReadContent( Wscript.Arguments(0) )

Sub ReadContent( strLyric )
	xHttp.Open "GET", "https://bitbucket.org/findwise/the-findwise-song/raw/db27a4814069f1adf5c26f99c8a69ad50c1309da/lyrics/findwise_song.lyrics", False
	xHttp.Send

	If xHttp.Status = 200 Then
		ObjVoice.Speak xHttp.responseText
	Else
		ObjVoice.Speak "I can not find that song."
	End If
End Sub
